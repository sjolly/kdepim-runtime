/*
    SPDX-FileCopyrightText: 2016 Sandro Knauß <sknauss@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef KDAV_DAVERROR_KDEPIM_RUNTIME_H
#define KDAV_DAVERROR_KDEPIM_RUNTIME_H

#include <KDAV/DavError>

QString translateErrorString(const KDAV::Error &error);

#endif
